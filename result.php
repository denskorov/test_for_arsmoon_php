<?php
require __DIR__ . '/models/Call.php';
require __DIR__ . '/models/Collection.php';
require __DIR__ . '/tools/IPFinder.php';
require __DIR__ . '/tools/PhoneFinder.php';

$file = $_FILES['file'];

$file = file($file['tmp_name']);
$csv = [];

foreach ($file as $row) {
    $item = explode(',', $row);
    $csv[] = new Call($item[0], $item[1], $item[2], $item[3], $item[4]);
}

$calls = new Collection($csv);

$phoneFinder = new PhoneFinder();

$ipFinder = new IPFinder();

$result = [];

while ($calls->valid()) {
    /** @var Call $call */
    $call = $calls->current();

    $c1 = $ipFinder->find($call->getIp());
    $c2 = $phoneFinder->find($call->getPhoneCode());

    if (!$result[$call->getId()]) {
        $result[$call->getId()] = [
            'id' => $call->getId(),
            'total_count' => 1,
            'total_duration' => $call->getDuration(),
            'continent_count' => $c1 === $c2 ? 1 : 0,
            'continent_duration' => $c1 === $c2 ? $call->getDuration() : 0,
        ];
    } else {
        $result[$call->getId()]['total_count']++;
        $result[$call->getId()]['total_duration'] += $call->getDuration();
        if ($c1 === $c2) $result[$call->getId()]['continent_count']++;
        if ($c1 === $c2) $result[$call->getId()]['continent_duration'] += $call->getDuration();
    }

    $calls->next();
}

echo '<table border="1" width="100%">';
foreach ($result as $item) {
    echo '<tr>';
    echo "<td>{$item['id']}</td>";
    echo "<td>{$item['total_count']}</td>";
    echo "<td>{$item['total_duration']}</td>";
    echo "<td>{$item['continent_count']}</td>";
    echo "<td>{$item['continent_duration']}</td>";
}
