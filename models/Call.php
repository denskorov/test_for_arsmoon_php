<?php

class Call
{
    private $id;
    private $date;
    private $duration;
    private $phone;
    private $ip;

    public function __construct($id, $date, $duration, $phone, $ip)
    {
        $this->id = $id;
        $this->date = $date;
        $this->duration = $duration;
        $this->phone = $phone;
        $this->ip = $ip;
    }

    function getPhoneCode(){
        return substr($this->phone, 0, 3);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return mixed
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return mixed
     */
    public function getIp()
    {
        return $this->ip;
    }
}
