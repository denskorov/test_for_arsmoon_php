<?php


class Collection implements Iterator
{
    public $collection;

    private $index = 0;

    public function __construct(array $collection = [])
    {
        $this->collection = $collection;
    }

    public function current()
    {
        return $this->collection[$this->index];
    }

    public function next()
    {
        ++$this->index;
    }

    public function key()
    {
        return $this->index;
    }

    public function valid()
    {
        return isset($this->collection[$this->index]);
    }

    public function rewind()
    {
        $this->index = 0;
    }
}
