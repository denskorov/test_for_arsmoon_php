<?php

require_once __DIR__ . '/IFinder.php';

class IPFinder implements IFinder
{
    private $accessKey = 'e1b70b46f5da774e4ffd8432de015692';

    public function find($value)
    {
        $url = 'http://api.ipstack.com/' . trim($value) . '?access_key=' . $this->accessKey;

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $json = curl_exec($ch);
        curl_close($ch);

        $api_result = json_decode($json, true);

        return $api_result['continent_code'];
    }
}
