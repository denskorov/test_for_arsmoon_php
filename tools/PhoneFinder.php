<?php

require_once __DIR__ . '/IFinder.php';

class PhoneFinder implements IFinder
{
    /**
     * @var array
     */
    private $countries;

    public function __construct()
    {
        $txt_file = file_get_contents('http://download.geonames.org/export/dump/countryInfo.txt');
        $rows = explode("\n", $txt_file);
        array_shift($rows);

        foreach ($rows as $row => $data) {
            if ($data[0] === '#') continue;

            $this->countries[] = explode("\t", $data);
        }
    }


    public function find($value)
    {
        $code = substr($value, 0, 3);
        $index = array_search($code, array_column($this->countries, '12'));

        return $this->countries[$index][8];
    }

}